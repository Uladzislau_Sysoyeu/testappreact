import React from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";
import Home from './components/Home';
import Actors from './components/Actors';

function App() {
  return (
    <Router>
      <div>
        <Route exact path="/" component={Home} />
        <Route path="/actors" component={Actors} />
      </div>
    </Router>
  );
}

export default App;
