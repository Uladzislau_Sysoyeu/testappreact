import React from 'react';
import { Link } from "react-router-dom";
import '../App.css';

function HeaderLinks() {
  return (
    <div className="App-links">
      <Link to='/'><h1>Home</h1></Link>
      <Link to='/actors'><h1>Actors</h1></Link>
    </div>
  );
}

export default HeaderLinks;
