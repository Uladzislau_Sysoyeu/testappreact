import React from 'react';
import logo from '../logo.svg';
import '../App.css';
import HeaderLinks from './HeaderLinks';

function Home() {
  return (
    <div className="App">
      <header className="App-header">
        <HeaderLinks />
        <img src={logo} className="App-logo" alt="logo" />
      </header>
    </div>
  );
}

export default Home;
