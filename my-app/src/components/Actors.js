import React, {Component} from 'react';
import HeaderLinks from './HeaderLinks';

class Actors extends Component {
  constructor(props) {
      super(props);

      this.state ={ data: {}, isLoading: true, error: null };
      this.textInput = React.createRef();
      this.addActor = this.addActor.bind(this);
  }

  addActor() {
    const { data } = this.state;
    this.setState({ data: [...data, { name: this.textInput.current.value }] });
    this.textInput.current.value = '';
  }

  deleteActor(el){
    const { data } = this.state;
    this.setState({ data: data.filter(item => item.name !== el.name) });
  }

  componentDidMount() {
      fetch('https://swapi.co/api/people/')
        .then(response => response.json())
        .then(actors => this.setState({data: actors.results, isLoading: false }))
        .catch(e => {
          console.log(e);
          this.setState({ isLoading: false, error: e });
        });
  }

  render() {
      const { data, isLoading, error } = this.state;
      
      if (isLoading) return <div>...Loading</div>;

      if (error) return <div>{`Error: ${error.message}`}</div>;

      return (
        <div>
          <HeaderLinks />
          <ul className="Actors">
            {data.map((item) => <li key={Math.random().toString(36).substr(2, 9)}>
            {item.name}
            <input
              type="button"
              value="Delete"
              onClick={() => this.deleteActor(item)}
            />
            </li>)}
          </ul>
          <div className="Actors-form">
            <input
            type="text"
            ref={this.textInput} />

            <input
              type="button"
              value="Add actor"
              onClick={this.addActor}
            />
          </div>
        </div>
      );
  }


}

export default Actors;
